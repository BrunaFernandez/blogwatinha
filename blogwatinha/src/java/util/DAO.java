/*
 * #################################
 * Autora: Bruna de Lima Fernandez.
 * #################################
 */
package util;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author bruuh_000
 */
public class DAO<Tipo> implements Serializable {
    @PersistenceContext(unitName="blogwatinhaPU")
    protected EntityManager entityManager;
    
    public void persistir(Tipo objeto){
        entityManager.persist(objeto);
    }
	
    public void alterar(Tipo objeto){
	entityManager.merge(objeto);
    }
}
