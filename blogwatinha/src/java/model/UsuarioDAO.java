/*
 * #################################
 * Autora: Bruna de Lima Fernandez.
 * #################################
 */
package model;

import entity.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import util.DAO;

/**
 *
 * @author bruuh_000
 */
public class UsuarioDAO extends DAO<Usuario> implements Serializable {
    
    public List<Usuario> listarUsuarios(){        
        try {
            TypedQuery<Usuario> query = entityManager.createQuery("SELECT u FROM Usuarios u", Usuario.class);
            return query.getResultList();
        } catch (Exception e) {
            return new ArrayList<Usuario>();
        }
    }
    
    public Usuario buscaUsuario(String login){
        try {
            TypedQuery<Usuario> query = entityManager.createQuery("SELECT u FROM Usuarios WHERE u.login = ?1", Usuario.class);
            query.setParameter(1, login);
            return query.getSingleResult();
        } catch (Exception e) {
            return new Usuario();
        }
    }
    
    public Usuario buscarUsuarioPorID(int id){
        try {
            TypedQuery<Usuario> query = entityManager.createQuery("SELECT u FROM Usuarios WHERE u.id = ?1", Usuario.class);
            query.setParameter(1, id);
            return query.getSingleResult();
        } catch (Exception e) {
            return new Usuario();
        }
    }
    
    public void removerUsuario(int id){
        try {
            Query query = entityManager.createNativeQuery("DELETE FROM usuarios WHERE id = ?1", Usuario.class);
            query.setParameter(1, id);
            query.executeUpdate();
        } catch (Exception e) {
            System.out.println("Erro: " + e.toString());
            e.printStackTrace();
        }
    }
}
