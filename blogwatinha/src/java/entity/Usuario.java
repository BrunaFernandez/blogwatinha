/*
 * #################################
 * Autora: Bruna de Lima Fernandez.
 * #################################
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Usuario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long usuarioID;
    private String usuarioLogin;
    private String usuarioSenha;
    private String usuarioEmail;
    @OneToMany(cascade = ALL, mappedBy = "publicacaoUsuario")
    private Collection<Publicacao> usuarioPublicacoes;

    public Long getUsuarioID() {
        return usuarioID;
    }

    public void setUsuarioID(Long usuarioID) {
        this.usuarioID = usuarioID;
    }

    public String getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(String usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }

    public String getUsuarioSenha() {
        return usuarioSenha;
    }

    public void setUsuarioSenha(String usuarioSenha) {
        this.usuarioSenha = usuarioSenha;
    }

    public String getUsuarioEmail() {
        return usuarioEmail;
    }

    public void setUsuarioEmail(String usuarioEmail) {
        this.usuarioEmail = usuarioEmail;
    }

    public Collection<Publicacao> getUsuarioPublicacoes() {
        return usuarioPublicacoes;
    }

    public void setUsuarioPublicacoes(Collection<Publicacao> usuarioPublicacoes) {
        this.usuarioPublicacoes = usuarioPublicacoes;
    }
    
}
