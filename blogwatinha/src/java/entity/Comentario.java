/*
 * #################################
 * Autora: Bruna de Lima Fernandez.
 * #################################
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
public class Comentario implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long comentarioID;
    private String comentarioTexto;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date comentarioData;
    @Temporal(javax.persistence.TemporalType.TIME)
    private Date comentarioHora;
    @ManyToOne
    private Usuario comentarioUsuario;
    @ManyToOne
    private Publicacao comentarioPublicacao;

    /**
     * @return the comentarioID
     */
    public Long getComentarioID() {
        return comentarioID;
    }

    /**
     * @param comentarioID the comentarioID to set
     */
    public void setComentarioID(Long comentarioID) {
        this.comentarioID = comentarioID;
    }

    /**
     * @return the comentarioTexto
     */
    public String getComentarioTexto() {
        return comentarioTexto;
    }

    /**
     * @param comentarioTexto the comentarioTexto to set
     */
    public void setComentarioTexto(String comentarioTexto) {
        this.comentarioTexto = comentarioTexto;
    }

    /**
     * @return the comentarioData
     */
    public Date getComentarioData() {
        return comentarioData;
    }

    /**
     * @param comentarioData the comentarioData to set
     */
    public void setComentarioData(Date comentarioData) {
        this.comentarioData = comentarioData;
    }

    /**
     * @return the comentarioHora
     */
    public Date getComentarioHora() {
        return comentarioHora;
    }

    /**
     * @param comentarioHora the comentarioHora to set
     */
    public void setComentarioHora(Date comentarioHora) {
        this.comentarioHora = comentarioHora;
    }

    /**
     * @return the comentarioUsuario
     */
    public Usuario getComentarioUsuario() {
        return comentarioUsuario;
    }

    /**
     * @param comentarioUsuario the comentarioUsuario to set
     */
    public void setComentarioUsuario(Usuario comentarioUsuario) {
        this.comentarioUsuario = comentarioUsuario;
    }

    /**
     * @return the comentarioPublicacao
     */
    public Publicacao getComentarioPublicacao() {
        return comentarioPublicacao;
    }

    /**
     * @param comentarioPublicacao the comentarioPublicacao to set
     */
    public void setComentarioPublicacao(Publicacao comentarioPublicacao) {
        this.comentarioPublicacao = comentarioPublicacao;
    }
   
}
 