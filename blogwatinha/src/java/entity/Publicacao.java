/*
 * #################################
 * Autora: Bruna de Lima Fernandez.
 * #################################
 */
package entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity
public class Publicacao implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long publicacaoID;
    private String publicacaoTitulo;
    private String publicacaoSubTitulo;
    private String publicacaoTexto;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dataPublicacao;
    @Temporal(javax.persistence.TemporalType.TIME)
    private Date horaPublicacao;
    @ManyToOne
    private Usuario publicacaoUsuario;
    @OneToMany(cascade = ALL, mappedBy = "comentarioPublicacao")
    private List<Comentario> publicacaoComentarios;

    public Long getPublicacaoID() {
        return publicacaoID;
    }

    public void setPublicacaoID(Long publicacaoID) {
        this.publicacaoID = publicacaoID;
    }

    public String getPublicacaoTitulo() {
        return publicacaoTitulo;
    }

    public void setPublicacaoTitulo(String publicacaoTitulo) {
        this.publicacaoTitulo = publicacaoTitulo;
    }

    public String getPublicacaoSubTitulo() {
        return publicacaoSubTitulo;
    }

    public void setPublicacaoSubTitulo(String publicacaoSubTitulo) {
        this.publicacaoSubTitulo = publicacaoSubTitulo;
    }

    public String getPublicacaoTexto() {
        return publicacaoTexto;
    }

    public void setPublicacaoTexto(String publicacaoTexto) {
        this.publicacaoTexto = publicacaoTexto;
    }

    public Date getDataPublicacao() {
        return dataPublicacao;
    }

    public void setDataPublicacao(Date dataPublicacao) {
        this.dataPublicacao = dataPublicacao;
    }

    public Date getHoraPublicacao() {
        return horaPublicacao;
    }

    public void setHoraPublicacao(Date horaPublicacao) {
        this.horaPublicacao = horaPublicacao;
    }

    public Usuario getPublicacaoUsuario() {
        return publicacaoUsuario;
    }

    public void setPublicacaoUsuario(Usuario publicacaoUsuario) {
        this.publicacaoUsuario = publicacaoUsuario;
    }

    /**
     * @return the publicacaoComentarios
     */
    public List<Comentario> getPublicacaoComentarios() {
        return publicacaoComentarios;
    }

    /**
     * @param publicacaoComentarios the publicacaoComentarios to set
     */
    public void setPublicacaoComentarios(List<Comentario> publicacaoComentarios) {
        this.publicacaoComentarios = publicacaoComentarios;
    }
    
}
